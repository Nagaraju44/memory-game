let gameContainer = document.getElementById("game")
let resultEl = document.getElementById("result")
let countEl = document.createElement("h1")
countEl.classList.add("counting")

let clicksEl = document.getElementById("clicks")
clicksEl.classList.add("clicks")
let buttonEl = document.getElementById("button");
let gameContainerElement = document.getElementById("gameContainer")
let startContainerEl = document.getElementById("startContainer")

let GIFS = ["./gifs/1.gif","./gifs/2.gif","./gifs/3.gif","./gifs/4.gif","./gifs/5.gif","./gifs/6.gif","./gifs/7.gif","./gifs/8.gif","./gifs/9.gif","./gifs/10.gif","./gifs/11.gif","./gifs/12.gif"]

function gameStart(){
    gameContainerElement.style.display = "block"
    startContainerEl.style.display = "none"
}

function shuffle(array) {
    let counter = array.length;
    while (counter > 0) {
    let index = Math.floor(Math.random() * counter);
    counter--;
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }
  return array;
}

let shuffledGifs = shuffle(GIFS)
let requiredGifs = [...shuffledGifs, ...shuffledGifs]

let requiredShuffledGifs = shuffle(requiredGifs)

function createDivsForColors(gifArray) {
    for (let index = 1; index <= gifArray.length; index++) {
        let newDiv = document.createElement("div");
        newDiv.style.backgroundColor = "#7f369c";
        newDiv.id = `${index}`
        newDiv.classList.add("card-container")
        newDiv.addEventListener("click", handleCardClick);
        gameContainer.append(newDiv);

        let gifEl = document.createElement('img')
        gifEl.src = gifArray[index-1]
        gifEl.id = `${index}`
        gifEl.classList.add("gif")
        newDiv.appendChild(gifEl)   
    }
}

let urlArray = []
let elementsArray = []
let fixedArray = []
let clicks = 0;

function handleCardClick(event) {
    // console.log("you clicked", event.target);
    // console.log(event.target.querySelector('img'))

    clicks += 1
    clicksEl.textContent = `Clicks: ${clicks}`
    console.log(clicks)
    event.target.querySelector('img').style.display = "block";

    elementsArray.push(event.target.querySelector('img'))
    urlArray.push(event.target.querySelector('img').src)
    let elements = document.getElementsByClassName("card-container")

    if (urlArray.length === 2) {
        for (let element of elements){
             element.removeEventListener("click", handleCardClick)
        }

        setTimeout (() =>{
            let isEqual = (urlArray[0] === urlArray[1])
            if (isEqual) {
                fixedArray.push(elementsArray[0].id)
                fixedArray.push(elementsArray[1].id)
                urlArray = []
                elementsArray = []
                
                if (fixedArray.length === 24){
                    let highScore = localStorage.getItem("HighScore")
                    if (highScore){
                        if (JSON.parse(highScore) < clicks){
                            resultEl.textContent = `Congratulations. You Did it in ${clicks} moves.`
                            resultEl.classList.add("result")
                        } else{
                            localStorage.setItem("HighScore", JSON.stringify(clicks))
                            resultEl.textContent = `Congratulations. You Did it in ${clicks} moves. You are the High scorer`
                            resultEl.classList.add("result")
                        }
                    } else{
                        localStorage.setItem("HighScore", JSON.stringify(clicks))
                        resultEl.textContent = `Congratulations. You Did it in ${clicks} moves. You are the High scorer`
                        resultEl.classList.add("result")
                    }
                    buttonEl.textContent = "Replay"
                    buttonEl.style.display = "block"
                }
            } else {
                elementsArray[0].style.display = "none";
                elementsArray[1].style.display = "none";
                urlArray = []
                elementsArray = []
            }
              
            for (let element of elements) {
                if (!(fixedArray.includes(element.id))){
                   element.addEventListener("click", handleCardClick)
                }
            }
        }, 1000)
    } else if (urlArray.length === 1){
      for (let element of elements){
          if (elementsArray[0].id === element.id){
              element.removeEventListener("click", handleCardClick)
          }
      }
    }
}


createDivsForColors(requiredShuffledGifs);